<?php
use cynthia15\pacote1\Exemplo;


describe('Exemplo', function() {
    describe('nome', function(){
        it('contem "pacote"', function() {
            $e = new Exemplo();
            expect($e->nome())->toContain('pacote');
        });
    });
});